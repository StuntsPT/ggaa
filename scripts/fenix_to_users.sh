#!/bin/bash

# This script converts an export from FENIX (3 columns: `username`;`student_number`;`Full name`) into a file ready to bulk add users to a GNU/Linux system using the command `newusers`
# Reads from STDIN and writes to STDOUT

# Define some variables
_DEFAULT_GROUP=5000
_UID_START=5000

# Main loop
while read students
do 
  echo "$(echo ${students} | cut -d "@" -f 1):$(echo ${students} | cut -d ";" -f 2 ):$(((_UID_START++))):${_DEFAULT_GROUP}:$(echo ${students} | cut -d ";" -f 3):/home/$(echo ${students} | cut -d "@" -f 1):/bin/bash"
done

# Usage: ./fenix_to_users.sh < Alunos.csv > newusers.csv
