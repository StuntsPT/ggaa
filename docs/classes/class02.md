### Linux shell 101

![Linux](C02_assets/linux.jpg)

© Francisco Pina Martins 2015-2021

---

### What is the shell?

* &shy;<!-- .element: class="fragment" -->A "command line" interface
* &shy;<!-- .element: class="fragment" --> *bash* (*ksh* *zsh* *fish* *dash* etc...)
* &shy;<!-- .element: class="fragment" --> Can be accessed via a "terminal"
* &shy;<!-- .element: class="fragment" --> **Mostly** keyboard driven
* &shy;<!-- .element: class="fragment" --> The "mouse cursor" is also useful here

---

### Why do I need it?

* &shy;<!-- .element: class="fragment" -->HPC clusters
* &shy;<!-- .element: class="fragment" -->Remote access
* &shy;<!-- .element: class="fragment" -->**Simple** interface
* &shy;<!-- .element: class="fragment" -->Process automation & reproducibility

---

## The *prompt*

``user@machine:~$``

``bash-4.1$``

![Prompt image](C02_assets/shell_prompt_small.gif)

[Pimp my prompt](https://www.cyberciti.biz/tips/howto-linux-unix-bash-shell-setup-prompt.html) <!-- .element: class="fragment" data-fragment-index="1" -->

---

### Navigation & orientation

<div style="float: right"><img src="C02_assets/shell_prompt_small.gif" /></div>

* &shy;<!-- .element: class="fragment" -->Where am I?
  * &shy;<!-- .element: class="fragment" -->``$ pwd`` 
  * &shy;<!-- .element: class="fragment" --><font color="red">p</font>rint <font color="red">w</font>orking <font color="red">d</font>irectory 
* &shy;<!-- .element: class="fragment" -->What is in here? 
  * &shy;<!-- .element: class="fragment" -->``$ ls`` 
  * &shy;<!-- .element: class="fragment" --><font color="red">l</font>i<font color="red">s</font>t 
* &shy;<!-- .element: class="fragment" -->How do I move around?
  * &shy;<!-- .element: class="fragment" -->``$ cd`` 
  * &shy;<!-- .element: class="fragment" --><font color="red">c</font>all <font color="red">d</font>irectory 

---

### Your new best freinds

* &shy;<!-- .element: class="fragment" -->``man program_name``
* &shy;<!-- .element: class="fragment" -->**EVERY** shell **core** program has it
* &shy;<!-- .element: class="fragment" -->Some others have it too

</br>

<div class="fragment" style="float: right"><img src="C02_assets/shell_prompt_small.gif" /></div>

</br>

* &shy;<!-- .element: class="fragment" -->``$ man ls``
* &shy;<!-- .element: class="fragment" -->``$ whatis ls``

---

### Useful keyboard navigation keys

<div class="fragment" style="float: right"><img src="C02_assets/Tab-Key.png" /></div>

* "Tab" Key
  * &shy;<!-- .element: class="fragment" -->Completes your command
  * &shy;<!-- .element: class="fragment" -->Try with one or two "*keypresses*"
* &shy;<!-- .element: class="fragment" -->↓ and ↑ keys
  * &shy;<!-- .element: class="fragment" -->Navigate previous commands

|||

### Useful keyboard navigation keys

```bash
cd  # Moves back to ~/

ls  # Lists current directory (~/) contents

ls Down<TAB>  # Completes the word "Downloads"
ls D<TAB><TAB>  # Shows all options starting with "D"
↑  # Pressing the up arrow reveals the previously entered command
```

---

### "The PATH"

* &shy;<!-- .element: class="fragment" -->Paths can be absolute...
  * &shy;<!-- .element: class="fragment" -->``/etc/fstab``
  * &shy;<!-- .element: class="fragment" -->``~/Downloads/ficheiro.txt``
* &shy;<!-- .element: class="fragment" -->Or relative...
  * &shy;<!-- .element: class="fragment" -->``Downloads/ficheiro.txt``
  * &shy;<!-- .element: class="fragment" -->``./ficheiro.txt``
* &shy;<!-- .element: class="fragment" --> Frequent convention examples
  * &shy;<!-- .element: class="fragment" --><font color="red">~</font> -> *home dir*, ex. ``/home/francisco``
  * &shy;<!-- .element: class="fragment" --><font color="red">./</font> -> "Here"
  * &shy;<!-- .element: class="fragment" --><font color="red">../</font> -> One directory below
  * &shy;<!-- .element: class="fragment" --><font color="red">../../</font> -> Two 'dirs' below
  * &shy;<!-- .element: class="fragment" --><font color="red">.file</font> -> "Hidden" file

|||

### The PATH

```bash
cd
ls /etc/fstab  # Lists a single file
ls ~/  # Lists contents of your home dir
touch ./aa  # Create a new file (or update date if already exists)
cd Downloads
ls ../
touch .aa
ls -a  # Now you see it
ls  # Now you don't
```

---

### Navigation part II

1. How do I create a new dir (AKA - folder)? <!-- .element: class="fragment" data-fragment-index="1" -->
2. How do I erase a dir? <!-- .element: class="fragment" data-fragment-index="2" -->
3. How do I create a new file? <!-- .element: class="fragment" data-fragment-index="3" -->
4. How do I copy a file? <!-- .element: class="fragment" data-fragment-index="4" -->
5. How do I move a file? <!-- .element: class="fragment" data-fragment-index="5" -->
6. How do I erase a file? <!-- .element: class="fragment" data-fragment-index="6" -->

<div style="float: right"><img src="C02_assets/shell_prompt_small.gif" /></div>

|||

### Navigation part II

```bash
mkdir dir_name
rmdir dir_name  # Dir needs to be empty!
touch file_name
cp origin destination
mv origin destination
rm file_name
```

<div class="fragment" style="color:red;">

Be **very**, **very** careful with `rm`

</div>

---

## Take 5'

---

## Permissions

#### Every file has "attributes"

```bash 
ls -l
drwxr-xr-x  5 francisco francisco 4096 Oct 22 00:24 Desktop
drwxr-xr-x  3 francisco cobig2    4096 2013-05-20 13:55 Databases
-rw-r--r--  1 francisco francisco 4256 Sep 15  2011 Zkill.py
```

* &shy;<!-- .element: class="fragment" -->3 scopes
  * &shy;<!-- .element: class="fragment" -->user
  * &shy;<!-- .element: class="fragment" -->group
  * &shy;<!-- .element: class="fragment" -->others
* &shy;<!-- .element: class="fragment" -->Each of which has 3 "values"
  * &shy;<!-- .element: class="fragment" --><font color="red">r</font>ead (4)
  * &shy;<!-- .element: class="fragment" --><font color="red">w</font>rite (2)
  * &shy;<!-- .element: class="fragment" -->e<font color="red">x</font>ecute (1)

---

### Permissions Part II

* &shy;<!-- .element: class="fragment" -->Changing ownership is simple:
  * &shy;<!-- .element: class="fragment" -->`$ chown user:group file`
* &shy;<!-- .element: class="fragment" -->Changing permissions... not so much:
  * &shy;<!-- .element: class="fragment" -->`chmod mode file`
    * &shy;<!-- .element: class="fragment" -->5 - read&execute (4+1)
    * &shy;<!-- .element: class="fragment" -->6 - read&write (4+2)
    * &shy;<!-- .element: class="fragment" -->7 - read&write&execute (4+2+1)
  * &shy;<!-- .element: class="fragment" -->Using trios of <font color="green">U</font><font color="blue">G</font><font color="red">O</font> (<font color="green">U</font>ser<font color="blue">G</font>roup<font color="red">O</font>thers)
    * &shy;<!-- .element: class="fragment" --><font color="green">7</font><font color="blue">7</font><font color="red">7</font> or <font color="green">7</font><font color="blue">5</font><font color="red">5</font> or <font color="green">6</font><font color="blue">4</font><font color="red">0</font>
  * &shy;<!-- .element: class="fragment" -->Or using a "verbal" form
    * &shy;<!-- .element: class="fragment" -->`chmod +x file`
    * &shy;<!-- .element: class="fragment" -->`chmod g-w file`

|||

### Permissions Part II

Try it:

```bash
groups  # Shows which groups your user belongs to
touch perm_test_file  # Create a new empty file for testing
# chmod and chown at will
```

---

### Environmental variables

Dynamically named values that can be "recalled"

```bash
test_var="hello"  # Defines the variable $test_var
echo $test_var  # What does `echo` do?
echo $USER  # Pre-defined variables
echo $HOME
# aliases are very similar:
alias ll="ls -l"
ll
alias hi="echo 'Hello there'"  # Notice the use of different quotation marks
```

<p class="fragment">Any env var or alias you set will be "forgotten" the moment you close your shell session.</p>

---

### Unix pipe: "|"

#### Command output can be used as input for a following program

```bash
ls /home
ls /home | grep "5"  # What is happening here?
ls /home | grep "5" | sort -r # What about here?
```

&shy;<!-- .element: class="fragment" -->![Unlimited power](C02_assets/unlimited.gif)

---

### Arguments & whitespace

* &shy;<!-- .element: class="fragment" -->A program and its arguments are always split by a "whitespace"
  * &shy;<!-- .element: class="fragment" -->`ls -l`
* &shy;<!-- .element: class="fragment" -->Frequently, more than one argument is "passed" to the program
  * &shy;<!-- .element: class="fragment" -->`cp origin destination`
* &shy;<!-- .element: class="fragment" -->What if there is whitespace in a filename?
  * &shy;<!-- .element: class="fragment" -->`filename with spaces.txt`
  * &shy;<!-- .element: class="fragment" -->`cat filename\ with\ spaces.txt`
  * &shy;<!-- .element: class="fragment" -->`cat 'filename with spaces.txt'`

---

### Awesome starts here

#### ``wget/curl grep sed cat less vim nano git``

Consider this example [FASTA file](C02_assets/example.fasta)?

```bash
wget https://gitlab.com/StuntsPT/ggaa/-/raw/main/docs/classes/C02_assets/example.fasta  # See also `curl`
cat example.fasta
grep ">" example.fasta
sed 's/>/+/g' example.fasta
less example.fasta  # Press 'q' to exit less and return to the shell prompt
nano example.fasta
```

&shy;<!-- .element: class="fragment" -->[Only the brave will be able to use Vim...](https://vim-adventures.com/)

---

### *Wildcards*

* &shy;<!-- .element: class="fragment" -->Special characters that mean something other than their "literal"
  * &shy;<!-- .element: class="fragment" -->"\*" - any character any number of times
  * &shy;<!-- .element: class="fragment" -->"?" - any character, once

|||

### *Wildcards*

```bash
ls D*
cp *.fasta ~/my_fastas_dir
```

---

### *Redirects*

* &shy;<!-- .element: class="fragment" -->Represented by the `>` character
* &shy;<!-- .element: class="fragment" -->Very similar to `|`, but *redirects* a program's output into a file instead of another program
* &shy;<!-- .element: class="fragment" -->Can be used in may ways: 

<div class="fragment">

```bash
ls -l > file.txt  # resets a file to 0 bytes and writes to it
ls -l >> file.txt  # appends text to the end of the file
```

</div>

---

### Combos

```bash
cat example.fasta | grep ">"  > sequence_titles.txt
# Even better:
cat example.fasta |grep ">" | sed 's/>//g' > sequence_names.txt
# Who can figure this one out?
```

---

### How do redirects work?

* &shy;<!-- .element: class="fragment" -->`stdin`
* &shy;<!-- .element: class="fragment" -->`stdout`
* &shy;<!-- .element: class="fragment" -->`stderr`
  * &shy;<!-- .element: class="fragment" -->`0<` or `<`
  * &shy;<!-- .element: class="fragment" -->`>1` or `>`
  * &shy;<!-- .element: class="fragment" -->`>2`
* &shy;<!-- .element: class="fragment" -->Be **very** careful when using ">" in shell commands

|||

### How do redirects work?

```bash
tr "T" "U" < example.fasta
tr "T" "U" < example.fasta > example_RNA.fasta
python example.fasta
python example.fasta > nope.txt
python example.fasta 2> yup.txt
# Check the contents of these 2 files and tell me what happened
```

---

### (De)compressing files

<div style="float: right"><img src="C02_assets/shell_prompt_small.gif" /></div>

* &shy;<!-- .element: class="fragment" -->Zip files
  * &shy;<!-- .element: class="fragment" -->`unzip file.zip`
* &shy;<!-- .element: class="fragment" -->"tar" files
  * &shy;<!-- .element: class="fragment" -->`tar xvfz file.tar.gz`
  * &shy;<!-- .element: class="fragment" -->`tar xvfj file.tar.bz2`
  * &shy;<!-- .element: class="fragment" -->`tar xvfJ file.tar.xz`
* &shy;<!-- .element: class="fragment" -->We can also use `tar` to compress
  * &shy;<!-- .element: class="fragment" -->`tar cvfJ file.tar.xz dir_to_compress`
* &shy;<!-- .element: class="fragment" -->We can use `gzip` to compress directly from `stdin`
  * &shy;<!-- .element: class="fragment" -->`cat file.txt | gzip > file.txt.gz`
  * &shy;<!-- .element: class="fragment" -->(Use `gunzip` to uncompress)

---

### Take 5' more

---

### Remote access

* &shy;<!-- .element: class="fragment" -->You will now remotely connect to one of our servers
  * &shy;<!-- .element: class="fragment" -->odin.fc.ul.pt
  * &shy;<!-- .element: class="fragment" -->loki.fc.ul.pt
  * &shy;<!-- .element: class="fragment" -->thor.fc.ul.pt
* &shy;<!-- .element: class="fragment" -->`ssh`

<div class="fragment">

```bash
ssh user@host  # Genreic form
ssh fc29609@odin.fc.ul.pt  # Specific form
```
* Exit with the command `exit` or with `Ctrl+D`

</div>

|||

### Remote access from MS Windows

<div style="float: left; width:50%">

* &shy;<!-- .element: class="fragment" -->Use a program called *Putty*
  * &shy;<!-- .element: class="fragment" -->"Host name" == "odin.fc.ul.pt"
  * &shy;<!-- .element: class="fragment" -->You will be asked for your credentials on login

</div>
<div style="float: right; width:50%">

&shy;<!-- .element: class="fragment" -->![Putty connection interface](C02_assets/putty.png)

</div>

---

### Getting data

* &shy;<!-- .element: class="fragment" -->Copy data from the data directory:
  * &shy;<!-- .element: class="fragment" -->`/home/data/rookie`

<div class="fragment">

```bash
cp /home/data/rookie/test.fastq ~/
```

</div>

---

### Copying remote data

* &shy;<!-- .element: class="fragment" -->`scp origin destination`
  * &shy;<!-- .element: class="fragment" -->Use `:` to provide a path
  * &shy;<!-- .element: class="fragment" -->Can also use the `-r` switch to copy dirs

<div class="fragment">

```bash
scp username@host:example.fasta ./  # copy from the remote machine to the local machine
scp ./example.fasta username@host:  # copy from the local machine to the remote machine
```

</div>

---

### Long tasks

* &shy;<!-- .element: class="fragment" -->We need a way to leave our tasks running in the *background*
  * &shy;<!-- .element: class="fragment" -->`GNU Screen`
  * &shy;<!-- .element: class="fragment" -->`tmux`
* &shy;<!-- .element: class="fragment" -->*Terminal multiplexers*
* &shy;<!-- .element: class="fragment" -->Run *processes* in multiple *windows*

|||

### GNU Screen

```bash
screen -S my_session  # Create a new screen session
htop  # Start a program to leave running in the background
<Ctrl>+a, c  # Create a new window
<Ctrl>+a, <Ctrl>+a  # Cycle thru windows
<Ctrl>+a, d  # Detach session
screen -list  # View screen sessions
screen -dr my_session  # Resume using 'my_session'
exit  # Close the shell (and the screen session if all windows are closed)
```

---

### Integrity checks

* &shy;<!-- .element: class="fragment" -->When dealing with large files it is important to verify that copies are performed correctly
* &shy;<!-- .element: class="fragment" -->For this we use a form of "fingerprinting" or "hashes"
  * &shy;<!-- .element: class="fragment" -->`md5sum`
  * &shy;<!-- .element: class="fragment" -->`sha256sum`
* &shy;<!-- .element: class="fragment" -->Provide a unique value for each file

<div class="fragment">

```bash
md5sum example.fasta
sha256sum example.fasta
```

</div>

---

### Finding files

* &shy;<!-- .element: class="fragment" -->Sometimes it is necessary to find missing files
  * &shy;<!-- .element: class="fragment" -->`GNU find`
  * &shy;<!-- .element: class="fragment" -->Provide a filename (full or partial)
  * &shy;<!-- .element: class="fragment" -->Provide a search path
  * &shy;<!-- .element: class="fragment" -->Profit!

<div class="fragment">

```bash
find ~/ -name \*.fasta  # Find all FASTA files in your home dir
```

</div>

---

### References

* [What is the linux shell](https://bash.cyberciti.biz/guide/What_is_Linux_Shell)
* [The $PATH variable](https://www.linux.com/answers/what-purpose-path-variable)
* [File permissions in Linux](https://www.linux.com/learn/understanding-linux-file-permissions)
* [Stdin and Stdout](http://www.learnlinux.org.za/courses/build/shell-scripting/ch01s04.html)
* [`tar` manpage](https://linux.die.net/man/1/tar)
* [Bashcrawl](https://gitlab.com/slackermedia/bashcrawl)
* [Linux commands cheat sheet](C02_assets/linux_commands.pdf)
* [GNU Screen cheat sheet](C02_assets/gnu_screen.pdf)

