### Quality control and read mapping

![Linux](C03_assets/logo-FCUL.png)

© Francisco Pina Martins 2021

---

### FastQ format

* &shy;<!-- .element: class="fragment" -->Most of the times HTS data is provided as *reads*
* &shy;<!-- .element: class="fragment" -->Format of choice is `FastQ`

<div class="fragment">

```
@SRR17045170.1 1 length=232
GGTCAATATGGTGCAAAATAACATGAGTATCACGTGTGATGCGTTACTTTCCGGTAAAAATAAAAATGGTAAATAGAGCGCAATATCGGTAATAACATCACCAGTCTCATTCAAAATTGCCCCAATCCGTGTTTGCTGATTGCACTCACGCGCCAACATGCCATCCAGCGCATTGAGCGCCATACGGATAAAAAGCACGATGGTCAATAGCATAAAGAGGATGGGTTGTGCC
+SRR17045170.1 1 length=232
11AAAFFFFFFFFBGG11B11A1D111D3DA3A000B01D210AE0AFGGDA//A//121B11111B11D221D21111////B1FE/////B22@1B111010BBGG2BG22221BF111>///00</?/BFG01F11BF111<F00</0//<//?/?11<1?<<110-<-<C0000--<.<0/...;.0000../:-.:..09;0;0009;00000/..;/..;C-A-//
@SRR17045170.2 2 length=251
GCAAACGGTTCGCCGTTCTGTTCCATTTGCAGGTACGCCCGCGCATTACCAATAATTGCCTGAATTTTCCCTCGATGGCGGATAATCCCGGCGTCCTGTACCAGTCTTTCGACATCCTCTTCCTGCATTGCTGCGACCTTCACCGGATCGAACTGATGAAAGCAGGCGCGATAGTTTTCGCGTTTTTTGAGGACGGTGATCCACGATAACCCAGCCTGCTGCCCTTCATGGCAGATCATTTCGAACAGTTT
+SRR17045170.2 2 length=251
?BBBBFBBBABDGGGGEFFFGGHF5FGHAE532B552A2A20000BE5BA53B33FG55BF323DFHHB3@FE?1B412///>/3F4B?//</>@<B2G2?222??GHHH//?/?DFGFGGD<G<10FG1=GBDC-<<EHF0D<.--::../:;:0;00000///.9-9--;.9CCGGB-.9DBFEFF-.../:--;..BBB/9.-;..9;99.9.BA.F9999FFB////./../9/;BFF...9;/9FF
```

</div>

* &shy;<!-- .element: class="fragment" -->Contains both base calls and confidence
* &shy;<!-- .element: class="fragment" -->But is it fit for purpose?

---

### Dirty, dirty data!

* &shy;<!-- .element: class="fragment" -->To solve the problem it was generated to solve, data needs to conform to some checks:
  * &shy;<!-- .element: class="fragment" -->Base quality
  * &shy;<!-- .element: class="fragment" -->Sequence content
  * &shy;<!-- .element: class="fragment" -->Over representation

---

### FastQC

* &shy;<!-- .element: class="fragment" -->A quality control application for HTS data
* &shy;<!-- .element: class="fragment" -->[Find it here](http://www.bioinformatics.babraham.ac.uk/projects/download.html#fastqc)
* &shy;<!-- .element: class="fragment" -->It has become so popular that there are several clones
  * &shy;<!-- .element: class="fragment" -->[FastQT](https://github.com/labsquare/fastQt) (`C++` and `Qt5` clone)
  * &shy;<!-- .element: class="fragment" -->[FastQC-rs](https://github.com/fastqc-rs/fastqc-rs) (`rust` clone)

&shy;<!-- .element: class="fragment" -->![FastQC](C03_assets/babraham_bioinformatics.gif)

---

### Running FastQC

<div style="float:left; width:75%">

* &shy;<!-- .element: class="fragment" data-fragment-index="1"-->Can run in *interactive* mode (with a GUI)
  * &shy;<!-- .element: class="fragment" data-fragment-index="2"-->`fastqc`
* &shy;<!-- .element: class="fragment" data-fragment-index="3"-->Can run in batch mode
  * &shy;<!-- .element: class="fragment" data-fragment-index="4"-->`fastqc file1.fastq.gz file2.fastq.gz filen.fastq.gz`
  * &shy;<!-- .element: class="fragment" data-fragment-index="6"-->Gz extension is optional
* &shy;<!-- .element: class="fragment" data-fragment-index="7"-->Outputs an HTML report for each `fastq` file

</div>
<div class="fragment" data-fragment-index="5" style="float:right; width:25%">

![Drake meme](C03_assets/drake.jpg)

</div>

---

### FastQC examples

![Example 1](C03_assets/fastqc_ex01.png)

|||

### FastQC examples

![Example 2](C03_assets/fastqc_ex02.png)

|||

### FastQC examples

![Example 3](C03_assets/fastqc_ex03.png)

|||

### FastQC examples

![Example 4](C03_assets/fastqc_ex04.png)

|||

### FastQC examples

![Example 5](C03_assets/fastqc_ex05.png)

|||

### FastQC examples

![Example 6](C03_assets/fastqc_ex06.png)

|||

### FastQC examples

![Example 7](C03_assets/fastqc_ex07.png)

|||

### FastQC examples

![Example 8](C03_assets/fastqc_ex08.png)

|||

### FastQC examples

![Example 9](C03_assets/fastqc_ex09.png)

|||

### FastQC examples

![Example 10](C03_assets/fastqc_ex10.png)

|||

### FastQC examples

[A tutorial!](https://www.youtube.com/watch?v=bz93ReOv87Y)

---

### Using FastQC

* Run on the **remote** machine:

```bash
# Keep thing organized
mkdir sars_map
cd sars_map
# Obtain data
cp -r /home/data/sars/* ./
# Verify integrity
sha256sum delta/*
cat delta/checksums.txt
# Run FastQC
cd delta
fastqc *.fastq
```

* Run on the **local** machine

```bash
# Copy data to local machine
scp username@remote_machine:sars_map/delta/*.html ./
# View it!
firefox *.html  # Or just click the files using your file manager
```

---

### Issue correction

* &shy;<!-- .element: class="fragment" -->So... data isn't perfect...
* &shy;<!-- .element: class="fragment" -->It needs some "trimming"
* &shy;<!-- .element: class="fragment" -->Presenting... [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic)

|||

### Trimmomatic at work

![Trimmomatic workflow](C03_assets/trimmomatic_workflow.png)

---

### Running Trimmomatic

```bash
cd ~/sars_map/delta
TrimmomaticPE -phred33 SRR15571382-delta_1.fastq SRR15571382-delta_2.fastq \
SRR15571382-delta_P_1.fastq.gz SRR15571382-delta_U_1.fastq.gz \
SRR15571382-delta_P_2.fastq.gz SRR15571382-delta_U_2.fastq.gz \
ILLUMINACLIP:/opt/Trimmomatic/adapters/TruSeq3-PE.fa:2:30:10 \
LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 \
HEADCROP:15 CROP:220 

# What these lines do - in order:
# Quality type and input files
# Paired and unpaired "pair1" clean files
# Paired and unpaired "pair2" clean files
# Trimm adapters
# Discard low quality and short reads
# Cut first 15 bases and trim to 220 max length

# ALTERNATIVE as a single line (either run the above lines, OR this one)
# NOT BOTH!
TrimmomaticPE -phred33 SRR15571382-delta_1.fastq SRR15571382-delta_2.fastq SRR15571382-delta_P_1.fastq.gz SRR15571382-delta_U_1.fastq.gz SRR15571382-delta_P_2.fastq.gz SRR15571382-delta_U_2.fastq.gz ILLUMINACLIP:/opt/Trimmomatic/adapters/TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 HEADCROP:15 CROP:220
```

---

### Did it work?

* Run on the **remote** machine:

```bash
fastqc *.fastq.gz
```

* Run on the **local** machine

```bash
# Copy data to local machine
scp username@remote_machine:sars_map/delta/*.html ./
# View it!
firefox *_P_*.html  # Now we want to compare the previous files with the "_P" files
```

---

### Let the games begin

* &shy;<!-- .element: class="fragment" -->Now, we map our "clean" data to a reference genome!
* &shy;<!-- .element: class="fragment" -->For this, we use [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)

&shy;<!-- .element: class="fragment" -->![Bowtie2](C03_assets/bowtie2.png)

---

### Mapping our reads

* Organize our data files

```bash
cd ~/sars_map
mkdir delta_mapping
cd delta_mapping
cp ../*.fasta ./
cp ../delta/*_P_*.fas* ./
```

* Start the mapping

```bash
bowtie2-build sars-cov-2-reference.fasta SARS-COV-2  # Build an index
bowtie2 -x SARS-COV-2 -1 pair1.fastq.gz -2 pair2.fastq.gz -S delta_map.sam  # Map the reads. Careful with the filenames!
samtools view -bS delta_map.sam | samtools sort - -o delta_map.bam  # Convert sam -> bam
samtools index delta_map.bam  # Create an index for downstream programs
```

---

### So, what did I just do?

* &shy;<!-- .element: class="fragment" -->Next, we take a look at our results using "[tablet](https://ics.hutton.ac.uk/tablet/download-tablet/)"
* &shy;<!-- .element: class="fragment" -->Open a new terminal and login to the HPC server of choice, with the `-Y` switch: `ssh -Y username@remote`

<div class="fragment">

```bash
tablet
```

</div>

* &shy;<!-- .element: class="fragment" -->Select the `.bam` file as "input" and the `.fasta` file as a reference
* &shy;<!-- .element: class="fragment" -->Look at the provided GFF3 file to find the "spike" protein coordinates!
  * &shy;<!-- .element: class="fragment" -->*Hint hint*: `grep`

---

### References

* [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
* [Trimmomatic](https://usadellab.org/cms/?page=trimmomatic)
* [Trimmomatic Paper](https://dx.doi.org/10.1093%2Fbioinformatics%2Fbtu170)
* [AfterQC, a Trimmomatic alternative](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-017-1469-3)
* [fastp, another alternative](https://doi.org/10.1093/bioinformatics/bty560)
* [bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
* [Tablet](https://ics.hutton.ac.uk/tablet/)
* [SARS-COV-2 reference](https://www.ncbi.nlm.nih.gov/nuccore/NC_045512.2)
