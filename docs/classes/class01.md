### Class #1

#### Genética e Genómica das Alterações Ambientais 2021-2022

<img src="C01_assets/logo-FCUL.png" style="background:none; border:none; box-shadow:none;">

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

## Avaliação

* &shy; <!-- .element: class="fragment"-->Exame prático - 30%
* &shy; <!-- .element: class="fragment"-->Seminários apresentados pelos alunos - 40%
* &shy; <!-- .element: class="fragment"-->Participação - 30%

---

## Exame TP

* &shy; <!-- .element: class="fragment"-->Individual
* &shy; <!-- .element: class="fragment"-->Realizado em computador
* &shy; <!-- .element: class="fragment"-->Exercícios semelhantes aos das aulas teórico-práticas
* &shy; <!-- .element: class="fragment"-->**30% da nota total**
* &shy; <!-- .element: class="fragment"--><font color='red'>Nota >50% para passar</font>

---

## Seminários

* &shy; <!-- .element: class="fragment"-->Apresentações individual de 20 minutos
  * &shy; <!-- .element: class="fragment"-->Discussão de 5-10 minutos
* &shy; <!-- .element: class="fragment"-->2 talks por aluno
* &shy; <!-- .element: class="fragment"-->**40% da nota total**
* &shy; <!-- .element: class="fragment"--><font color='red'>Nota >50% para passar</font>

|||

## Talk 01 - Conceitos Fundamentais

* &shy; <!-- .element: class="fragment"-->Escolher um tema da [lista fornecida](#/4)
* &shy; <!-- .element: class="fragment"-->Envio de PDF pós-talk
* &shy; <!-- .element: class="fragment"-->Referências no último slide (com links)

|||

## Talk 02 - Conceitos Avançados

* &shy; <!-- .element: class="fragment"-->Escolher um tema da [lista fornecida](#/4/1)
* &shy; <!-- .element: class="fragment"-->Envio de PDF pós-talk
* &shy; <!-- .element: class="fragment"-->Referências no último slide (com links)

|||

## Talk 03 - *Case studies*

* &shy; <!-- .element: class="fragment"-->Tema "livre" (dentro dos tópicos da aula)
  * &shy; <!-- .element: class="fragment"-->Três artigos relacionados
  * &shy; <!-- .element: class="fragment"-->Enviem links com antecedência para aprovação
* &shy; <!-- .element: class="fragment"-->Envio de PDF pós-talk
* &shy; <!-- .element: class="fragment"-->Referências no último slide (com links)

---

## Lista de temas Talk 01

* Neutral theory
* Balancing selection
* Fst and related measures
* Tajima D test and related measures
* Genetic drift and population size
* Selective sweeps
* Linkage disequilibrium
* Demography and selection
* Effective population size
* HWE, causa dos desvios
* Population structure

|||

## Lista de temas Talk 02

* Lineage sorting 
* Backgroud selection
* Countergradient variation
* Inbreeding 
* Isolation-by-environment (IBE)
* Genetic assimilation
* Genetic/evolutionary rescue
* Genetic load
* Epistasis 
* Associative overdominance
* Epigenetics and adaptation
* Site Frequency Spectrum

---

## Participação

* &shy; <!-- .element: class="fragment"-->Participação no Journal Club
* &shy; <!-- .element: class="fragment"-->Participação nas aulas
* &shy; <!-- .element: class="fragment"-->Participação na discussão das talks
* &shy; <!-- .element: class="fragment"-->**30% da nota total**
* &shy; <!-- .element: class="fragment"--><font color='red'>Nota >50% para passar</font>

---

## Melhorias

* &shy; <!-- .element: class="fragment"-->Exame no último dia da UC, segunda data na época de recorrência
* &shy; <!-- .element: class="fragment"-->As notas de cada componente são guardadas
* &shy; <!-- .element: class="fragment"-->Para a melhoria usam-se as melhores notas parcelares

---

## Programa

**1ª semana**

* <font color="forestgreen">29-11-2021</font> – Apresentação; Enquadramento dos principais problemas associados às Alterações Ambientais e consequências evolutivas
* <font color="forestgreen">30-11-2021</font> – Tecnologias de Sequenciação; GNU/Linux shell
* <font color="purple">01-12-2021</font> **Feriado**
* <font color="forestgreen">02-12-2021</font> – Abordagens metodológicas aplicadas ao estudo das consequências evolutivas das AA; FastQC/Trimmomatic/mapping
* <font color="forestgreen">03-12-2021</font> – Deteção de assinaturas de Seleção Natural; ipyrad

|||

## Programa

**2ª semana**

* <font color="cyan">06-12-2021</font> – Talks 01
* <font color="forestgreen">07-12-2021</font> – *Local Adaptation*; Structure/Bayescan/PCA
* <font color="purple">08-12-2021</font> **Feriado**
* <font color="cyan">09-12-2021</font> – Talks 02
* <font color="pink">10-12-2021</font> – (**14:30**) Journal Club; Treino

|||

## Programa

**3ª semana**

* <font color="forestgreen">13-12-2021</font> – *Rapid Adaptation*; Treino
* <font color="cyan">14-12-2021</font> (**2.3.16**) – Talks 03
* <font color="cyan">15-12-2021</font> (**2.3.16**) – Talks 03
* <font color="yellow">16-12-2021</font> –(**15:30**) Treino
* <font color="orange">17-12-2021</font> – **Exame TP**

---

### Perguntas?
